/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import syslivraria.models.Artigo;
import syslivraria.models.Exemplar;
import syslivraria.models.Livro;
import syslivraria.models.Periodico;

/**
 *
 * @author Daniel
 */
public class ExemplarDAO implements DAO<Exemplar> {

    @Override
    public void inserir(Exemplar exemplar) throws Exception {

        DAO dao = null;

        if (exemplar instanceof Livro) {
            dao = new LivroDAO();
        } else if (exemplar instanceof Periodico) {
            dao = new PeriodicoDAO();
        } else if (exemplar instanceof Artigo) {
            dao = new ArtigoDAO();
        }

        dao.inserir(exemplar);

    }

    @Override
    public void atualizar(Exemplar exemplar) throws Exception {
        DAO dao = null;

        if (exemplar instanceof Livro) {
            dao = new LivroDAO();
        } else if (exemplar instanceof Periodico) {
            dao = new PeriodicoDAO();
        } else if (exemplar instanceof Artigo) {
            dao = new ArtigoDAO();
        }

        dao.atualizar(exemplar);
    }

    @Override
    public void deletar(Exemplar objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Exemplar> getAll() throws Exception {
        Connection connection = null;

        List<Exemplar> exemplares = new ArrayList<>();
        try {
            connection = Conexao.getConnection();
            String query = "Select * from Exemplar";
            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {

                Exemplar exemplar = null;
                int id = rs.getInt(1);
                int tipo = rs.getInt(2);
                String titulo = rs.getString(4);
                String autor = rs.getString(5);
                String editora = rs.getString(6);
                int edicao = rs.getInt(7);
                boolean ativo = rs.getBoolean(8);

                switch (tipo) {
                    case 0:
                        exemplar = new Livro(id, titulo, autor, editora, edicao);
                        break;
                    case 1:
                        exemplar = new Periodico(id, titulo, editora);
                        break;
                    case 2:
                        exemplar = new Artigo(id, titulo, autor);

                }

                exemplar.setAtivo(ativo);
                exemplares.add(exemplar);

            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return exemplares;
    }

    public Exemplar getById(int id) throws Exception {
        Connection connection = null;
        Exemplar exemplar = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select * from Exemplar where id = ?";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int tipo = rs.getInt(2);
                String titulo = rs.getString(4);
                String autor = rs.getString(5);
                String editora = rs.getString(6);
                int edicao = rs.getInt(7);
                boolean ativo = rs.getBoolean(8);

                switch (tipo) {
                    case 0:
                        exemplar = new Livro(id, titulo, autor, editora, edicao);
                        break;
                    case 1:
                        exemplar = new Periodico(id, titulo, editora);
                        break;
                    case 2:
                        exemplar = new Artigo(id, titulo, autor);

                }
                exemplar.setId(id);

            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return exemplar;
    }

    //

    public TableModel getAllResultSetTableModel() throws Exception {
        Connection connection = null;

        TableModel model = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select id , " +
                            "case  when tipoexemplar =1 then 'Livro' " + 
                            "when tipoexemplar = 2 then 'Artigo' " + 
                            "when tipoexemplar = 3 then  'Periódico' " + 
                            "end as Tipo , " + 
                            "codigo , Titulo , Autor  , Editora , Edicao , " + 
                            "case when ativo = 1 then 'Ativo' else 'Inativo' end " + 
                            "as 'situação' " + 
                            "from Exemplar " ; 
            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);
            model = ResultSetTableModel.resultSetToTableModel(rs);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return model;
    }

    public TableModel getAllResultSetTableModelFilter(String codigo, String tipo, String situacao, String titulo, String autor, String editora, String edicao) throws Exception {
        Connection connection = null;

        TableModel model = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select id , " +
                            "case  when tipoexemplar =1 then 'Livro' " + 
                            "when tipoexemplar = 2 then 'Artigo' " + 
                            "when tipoexemplar = 3 then  'Periódico' " + 
                            "end as Tipo , " + 
                            "codigo , Titulo , Autor  , Editora , Edicao , " + 
                            "case when ativo = 1 then 'Ativo' else 'Inativo' end " + 
                            "as 'situação' " + 
                            "from Exemplar " +
                            "Where 1=1 " ; 

            if (codigo != null) {
                query += " and id = " + codigo;
            }

            if (tipo != null) {
                query += " and tipoExemplar = " + tipo;
            }

            if (situacao != null) {
                query += " and ativo = " + situacao;
            }

            if (titulo != null) {
                query += " and Titulo like '%" + titulo + "%'";
            }

            if (autor != null) {
                query += " and autor = '%" + autor + "%'";
            }

            if (editora != null) {
                query += " and editora = '%" + editora + "%'";
            }

            if (edicao != null) {
                query += " and edicao = " + edicao;
            }

            System.out.println(query);

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);
            model = ResultSetTableModel.resultSetToTableModel(rs);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return model;
    }

}
