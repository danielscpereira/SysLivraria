/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import syslivraria.models.Periodico;

/**
 *
 * @author Daniel
 */
public class PeriodicoDAO implements DAO<Periodico> {

    @Override
    public void inserir(Periodico periodico) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "INSERT INTO exemplar (TipoExemplar , Codigo , Titulo , Autor , Editora , Edicao , Ativo) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, 1);
            ps.setInt(2, periodico.getCodigo());
            ps.setString(3, periodico.getTitulo());
            ps.setNull(4, Types.NULL);
            ps.setString(5, periodico.getEditora());
            ps.setNull(6, Types.NULL);
            ps.setBoolean(7, periodico.isAtivo());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int id = rs.getInt(1);
            periodico.setId(id);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void atualizar(Periodico periodico) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "UPDATE exemplar set "
                    + "TipoExemplar = ? , "
                    + "Codigo = ? , "
                    + "Titulo = ? , "
                    + "Autor = ? , "
                    + "Editora = ? , "
                    + "Edicao = ? , "
                    + "Ativo = ? "
                    + "WHERE Id = ?";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, 1);
            ps.setInt(2, periodico.getCodigo());
            ps.setString(3, periodico.getTitulo());
            ps.setNull(4, Types.NULL);
            ps.setString(5, periodico.getEditora());
            ps.setNull(6, Types.NULL);
            ps.setBoolean(7, periodico.isAtivo());
            ps.setInt(8, periodico.getId());

            ps.executeUpdate();

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void deletar(Periodico objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Periodico> getAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Periodico getById(int id) throws Exception {
        Connection connection = null;
        Periodico exemplar = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select titulo , editora , ativo  from Exemplar where id = ? and tipoExemplar = 1";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String titulo = rs.getString(1);
                String editora = rs.getString(2);
                boolean ativo = rs.getBoolean(3);
                exemplar = new Periodico(id, titulo, editora);
                exemplar.setId(id);
                exemplar.setAtivo(ativo);

            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return exemplar;
    }

}
