/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Daniel
 */
public class Conexao {

    public static Connection getConnection() {
        
        Properties p = new Properties();
        
        Connection con = null;
        try {
            
            //Carregando arquivo de propriedades
        
            p.load(Conexao.class.getResourceAsStream("database.properties"));

            // carregando classe de driver do banco
            Class.forName(p.getProperty("DRIVER"));

            // criando conexao
            con = DriverManager.getConnection(p.getProperty("URL"),
                    p.getProperty("USER"),
                    p.getProperty("PASSWORD"));
        } catch (IOException ex) {
            System.err.println("Erro ao carregar arquivo de configuração.." + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.err.println("Erro ao carregar driver do banco." + ex.getMessage());
        } catch (SQLException ex) {
            System.err.println("Erro ao conectar :" + ex.getMessage());
        }
        return con;
    }
    
    
    //metodo main para testar a conexao 
    public static void main(String args[]){
        
        Connection c = getConnection() ; 
        
    }

}
