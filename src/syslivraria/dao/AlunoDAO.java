/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.sql.*;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import syslivraria.models.Aluno;

/**
 *
 * @author sala306b
 */
public class AlunoDAO implements DAO<Aluno> {

    @Override
    public void inserir(Aluno aluno) throws Exception {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            connection.setAutoCommit(false);

            String query = "INSERT INTO endereco "
                    + "(Logradouro,Numero,Cep,Bairro,Cidade,Uf) values (?,?,?,?,?,?)";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, aluno.getEndereco().getLogradouro());
            ps.setInt(2, aluno.getEndereco().getNumero());
            ps.setString(3, aluno.getEndereco().getCep());
            ps.setString(4, aluno.getEndereco().getBairro());
            ps.setString(5, aluno.getEndereco().getCidade());
            ps.setString(6, aluno.getEndereco().getUf());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int idEndereco = rs.getInt(1);

            aluno.getEndereco().setId(idEndereco);

            query = "INSERT INTO cliente (Nome,Telefone,numeroMatricula,IdEndereco , Ativo) "
                    + "VALUES (?,?,?,?,?)";
            ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, aluno.getNome());
            ps.setString(2, aluno.getTelefone());
            ps.setInt(3, aluno.getNumeroMatricula());
            ps.setInt(4, aluno.getEndereco().getId());
            ps.setBoolean(5, aluno.isAtivo());

            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            rs.first();
            int id = rs.getInt(1);

            aluno.setId(id);

            connection.commit();

        } catch (Exception ex) {

            try {
                connection.rollback();
                throw ex;
            } catch (SQLException exSql) {
                throw exSql;
            }

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void atualizar(Aluno aluno) throws Exception {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            connection.setAutoCommit(false);

            String query = "UPDATE  endereco "
                    + "SET Logradouro = ? "
                    + ",Numero = ?"
                    + ",Cep = ?"
                    + ",Bairro = ?"
                    + ",Cidade = ?"
                    + ",Uf = ? WHERE ID = ?";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, aluno.getEndereco().getLogradouro());
            ps.setInt(2, aluno.getEndereco().getNumero());
            ps.setString(3, aluno.getEndereco().getCep());
            ps.setString(4, aluno.getEndereco().getBairro());
            ps.setString(5, aluno.getEndereco().getCidade());
            ps.setString(6, aluno.getEndereco().getUf());
            ps.setInt(7, aluno.getEndereco().getId());

            ps.executeUpdate();

            query = "UPDATE  cliente "
                    + "set Nome = ?"
                    + ",Telefone = ?"
                    + ",numeroMatricula = ?"
                    + ", Ativo = ? "
                    + "WHERE ID = ?";
            ps = connection.prepareStatement(query);

            ps.setString(1, aluno.getNome());
            ps.setString(2, aluno.getTelefone());
            ps.setInt(3, aluno.getNumeroMatricula());
            ps.setBoolean(4, aluno.isAtivo());
            ps.setInt(5, aluno.getId());

            ps.executeUpdate();

            connection.commit();

        } catch (Exception ex) {

            try {
                connection.rollback();
                throw ex;
            } catch (SQLException exSql) {
                throw exSql;
            }

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw ex;
            }
        }

    }

    @Override
    public void deletar(Aluno objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Aluno> getAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Aluno getById(int id) throws Exception {
        Connection connection = null;
        Aluno aluno = null;

        try {
            connection = Conexao.getConnection();
            String query = "SELECT C.ID , C.NOME , C.TELEFONE , C.numeroMatricula, C.ATIVO , "
                    + "E.Logradouro, E.Numero , E.Cep , E.Bairro , E.Cidade , E.Uf , C.IDENDERECO "
                    + "FROM CLIENTE C "
                    + "INNER JOIN ENDERECO E "
                    + "ON C.IdEndereco = E.Id "
                    + "WHERE C.Id = ? ";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                String nome = rs.getString(2);
                String telefone = rs.getString(3);
                int matricula = rs.getInt(4);
                boolean ativo = rs.getBoolean(5);
                String logradouro = rs.getString(6);
                int numero = rs.getInt(7);
                String cep = rs.getString(8);
                String bairro = rs.getString(9);
                String cidade = rs.getString(10);
                String uf = rs.getString(11);
                int idEndereco = rs.getInt(12);

                aluno = new Aluno(nome, telefone, matricula, cep, logradouro, numero, bairro, cidade, uf);
                aluno.setId(id);
                aluno.getEndereco().setId(idEndereco);
                aluno.setAtivo(ativo);

            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return aluno;

    }

    public TableModel getFilterResultSetTableModel(String codigo, String matricula, String situacao, String nome) throws Exception {

        List<String> clauses = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();

        Connection connection = null;

        TableModel model = null;

        try {
            connection = Conexao.getConnection();

            String query = "select c.id as codigo , c.numeroMatricula as Matrícula , c.Nome , " +
                           "case ativo when 1 then 'Ativo' else 'Inativo' end as Situacao " +
                           "from cliente c where 1 = 1 ";

            if (codigo != null) {
                clauses.add(" and c.id = ?");
                parameters.add(codigo);
            }

            if (matricula != null) {
                clauses.add(" and c.numeroMatricula = ?");
                parameters.add(matricula);
            }

            if (situacao != null) {
                clauses.add(" and c.ativo = ?");
                parameters.add(situacao);
            }

            if (nome != null) {
                clauses.add(" and c.nome like %?%");
                parameters.add(nome);
            }

            for (int i = 0; i < clauses.size(); i++) {
                query += clauses.get(i);
            }

            PreparedStatement ps = connection.prepareStatement(query);

            for (int i = 0; i < parameters.size(); i++) {
                ps.setObject(i + 1, parameters.get(i));
            }
            
            System.out.println(ps);

            ResultSet rs = ps.executeQuery();

            model = ResultSetTableModel.resultSetToTableModel(rs);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return model;
    }

}
