/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syslivraria.dao;

import java.util.List;

/**
 *
 * @author Daniel
 * @param <T>
 */
public interface DAO<T> {
    
    void inserir(T objeto) throws Exception;
    void atualizar(T objeto)throws Exception;
    void deletar(T objeto)throws Exception;
    List<T> getAll()throws Exception;
    T getById(int id) throws Exception;
    
}
