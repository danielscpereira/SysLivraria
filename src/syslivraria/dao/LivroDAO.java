/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import syslivraria.models.Livro;

/**
 *
 * @author Daniel
 */
public class LivroDAO implements DAO<Livro> {

    @Override
    public void inserir(Livro livro) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "INSERT INTO exemplar (TipoExemplar , Codigo , Titulo , Autor , Editora , Edicao , Ativo) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, 0);
            ps.setInt(2, livro.getCodigo());
            ps.setString(3, livro.getTitulo());
            ps.setString(4, livro.getAutor());
            ps.setString(5, livro.getEditora());
            ps.setInt(6, livro.getEdicao());
            ps.setBoolean(7, livro.isAtivo());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int id = rs.getInt(1);
            livro.setId(id);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void atualizar(Livro livro) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "UPDATE exemplar set "
                    + "TipoExemplar = ? , "
                    + "Codigo = ? , "
                    + "Titulo = ? , "
                    + "Autor = ? , "
                    + "Editora = ? , "
                    + "Edicao = ? , "
                    + "Ativo = ? "
                    + "WHERE Id = ?";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, 0);
            ps.setInt(2, livro.getCodigo());
            ps.setString(3, livro.getTitulo());
            ps.setString(4, livro.getAutor());
            ps.setString(5, livro.getEditora());
            ps.setInt(6, livro.getEdicao());
            ps.setBoolean(7, livro.isAtivo());
            ps.setInt(8, livro.getId());

            ps.executeUpdate();

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void deletar(Livro objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Livro> getAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Livro getById(int id) throws Exception {
        Connection connection = null;
        Livro exemplar = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select * from Exemplar where id = ? and tipoExemplar = 0";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int tipo = rs.getInt(2);
                String titulo = rs.getString(4);
                String autor = rs.getString(5);
                String editora = rs.getString(6);
                int edicao = rs.getInt(7);
                boolean ativo = rs.getBoolean(8);

                exemplar = new Livro(id, titulo, autor, editora, edicao);
                exemplar.setId(id);
                exemplar.setAtivo(ativo);

            }
            

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return exemplar;
    }

}
