/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import com.sun.org.apache.bcel.internal.generic.Type;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import syslivraria.models.Aluno;
import syslivraria.models.Cliente;
import syslivraria.models.Emprestimo;
import syslivraria.models.Exemplar;

/**
 *
 * @author Daniel
 */
public class EmprestimoDAO implements DAO<Emprestimo> {

    @Override
    public void inserir(Emprestimo emprestimo) throws Exception {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            connection.setAutoCommit(false);

            String query = "INSERT INTO Emprestimo "
                    + "(idCliente , dataEmprestimo) values (?,?)";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, emprestimo.getCliente().getId());
            Date dataEmprestimo = new Date(emprestimo.getDataEmprestimo().getTime());
            ps.setDate(2, dataEmprestimo);

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int idEmprestimo = rs.getInt(1);

            emprestimo.setId(idEmprestimo);

            query = "INSERT INTO itemEmprestimo (idExemplar,idEmprestimo) "
                    + "VALUES (?,?)";
            ps = connection.prepareStatement(query);

            for (Exemplar e : emprestimo.getExemplares()) {

                ps.setInt(1, e.getId());
                ps.setInt(2, idEmprestimo);
                System.out.println(ps);
                ps.executeUpdate();

            }

            connection.commit();

        } catch (Exception ex) {

            try {
                connection.rollback();
                throw ex;
            } catch (SQLException exSql) {
                throw exSql;
            }

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw ex;
            }
        }

    }

    @Override
    public void atualizar(Emprestimo emprestimo) throws Exception {
        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            String query = "Update Emprestimo "
                    + "set idCliente = ? ,"
                    + "dataEmprestimo = ?  , "
                    + "dataDevolucao = ? "
                    + "where id = ?";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, emprestimo.getCliente().getId());
            Date dataEmprestimo = new Date(emprestimo.getDataEmprestimo().getTime());
            ps.setDate(2, dataEmprestimo);

            if (emprestimo.getDataDevolucao() != null) {
                Date datadevolucao = new Date(emprestimo.getDataDevolucao().getTime());
                ps.setDate(3, datadevolucao);
            } else {
                ps.setNull(3, Types.NULL);
            }
            
            ps.setInt(4, emprestimo.getId());

            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void deletar(Emprestimo objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Emprestimo> getAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Emprestimo getById(int id) throws Exception {
        
        Connection connection = null;
        Emprestimo emprestimo = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select * from emprestimo where id = ?";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();

            rs.first();
            int idEmprestimo = rs.getInt(1);
            int idCliente = rs.getInt(2);
            java.util.Date dataEmprestimo = rs.getDate(3);
            java.util.Date dataDevolucao = rs.getDate(4);
            emprestimo = new Emprestimo(dataEmprestimo);
            emprestimo.setDataDevolucao(dataDevolucao);
            emprestimo.setId(idEmprestimo);
            
            Cliente cliente = new AlunoDAO().getById(idCliente);
            emprestimo.setCliente(cliente);
            
            //montando colecao 
            
            query = "Select idexemplar from itememprestimo where id = ?";
            ps = connection.prepareStatement(query);

            ps.setInt(1, id);
            rs = ps.executeQuery();
            ExemplarDAO exemplarDAO = new ExemplarDAO();
            
            while(rs.next()){
                Exemplar exemplar  = exemplarDAO.getById(rs.getInt(1));
                emprestimo.addExemplar(exemplar);
            }
            
            
            
            
            

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return emprestimo;

        
        
    }

    public TableModel getFilterResultSetTableModel(String numero, java.util.Date dateInicio, java.util.Date dataFim, String situacao, String nome) throws Exception {

        List<String> clauses = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();

        Connection connection = null;

        TableModel model = null;

        try {
            connection = Conexao.getConnection();

            String query = "select e.id as 'Número' , "
                    + "c.nome as 'Cliente' , "
                    + "DATE_FORMAT(e.dataemprestimo,'%d/%m/%Y') "
                    + "as 'Data Empréstimo' , "
                    + "DATE_FORMAT(e.datadevolucao,'%d/%m/%Y') "
                    + "as 'Data Devolução' , "
                    + "case when e.datadevolucao is null then 'Em Aberto' else 'Devolvido' end as 'Situação' "
                    + "from emprestimo e "
                    + "inner join cliente c "
                    + "on e.idcliente = c.id  "
                    + "Where 1 = 1 ";

            if (numero != null) {
                clauses.add(" and c.id = ?");
                parameters.add(numero);
            }

            if (dateInicio != null) {
                clauses.add(" and e.dataEmprestimo >= ?");
                parameters.add(dateInicio);
            }

            if (dataFim != null) {
                clauses.add(" and (e.datadevolucao <= ? or e.datadevolucao is null)");
                parameters.add(dataFim);
            }

            if (situacao != null) {

                if (situacao.equals("Devolvido")) {
                    clauses.add(" and e.datadevolucao is not null");
                } else {
                    clauses.add(" and e.datadevolucao is null");
                }

            }

            if (nome != null) {
                clauses.add(" and c.nome like '%" + nome + "%'");
            }

            for (int i = 0; i < clauses.size(); i++) {
                query += clauses.get(i);
            }

            PreparedStatement ps = connection.prepareStatement(query);

            for (int i = 0; i < parameters.size(); i++) {
                ps.setObject(i + 1, parameters.get(i));
            }

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();

            model = ResultSetTableModel.resultSetToTableModel(rs);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return model;
    }
}
