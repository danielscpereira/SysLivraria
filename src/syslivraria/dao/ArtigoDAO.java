/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import syslivraria.models.Artigo;
import syslivraria.models.Livro;

/**
 *
 * @author Daniel
 */
public class ArtigoDAO implements DAO<Artigo> {

    @Override
    public void inserir(Artigo artigo) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "INSERT INTO exemplar (TipoExemplar , Codigo , Titulo , Autor , Editora , Edicao , Ativo) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setInt(1, 2);
            ps.setInt(2, artigo.getCodigo());
            ps.setString(3, artigo.getTitulo());
            ps.setString(4, artigo.getAutor());
            ps.setNull(5, Types.NULL);
            ps.setNull(6, Types.NULL);
            ps.setBoolean(7, artigo.isAtivo());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int id = rs.getInt(1);
            artigo.setId(id);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void atualizar(Artigo artigo) throws Exception {
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String query = "UPDATE exemplar set "
                    + "TipoExemplar = ? , "
                    + "Codigo = ? , "
                    + "Titulo = ? , "
                    + "Autor = ? , "
                    + "Editora = ? , "
                    + "Edicao = ? , "
                    + "Ativo = ? "
                    + "WHERE Id = ?";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, 2);
            ps.setInt(2, artigo.getCodigo());
            ps.setString(3, artigo.getTitulo());
            ps.setString(4, artigo.getAutor());
            ps.setNull(5, Types.NULL);
            ps.setNull(6, Types.NULL);
            ps.setBoolean(7, artigo.isAtivo());
            ps.setInt(8, artigo.getId());

            ps.executeUpdate();

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }
    }

    @Override
    public void deletar(Artigo objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Artigo> getAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Artigo getById(int id) throws Exception {
        Connection connection = null;
        Artigo exemplar = null;

        try {
            connection = Conexao.getConnection();
            String query = "Select titulo , autor , ativo from Exemplar where id = ? and tipoExemplar = 2";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                String titulo = rs.getString(1);
                String autor = rs.getString(2);
                boolean ativo = rs.getBoolean(3);

                exemplar = new Artigo(id, titulo, autor);
                exemplar.setId(id);
                exemplar.setAtivo(ativo);

            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao fechar conexão..");
            }
        }

        return exemplar;

    }

}
