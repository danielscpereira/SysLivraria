/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

/**
 *
 * @author Daniel
 */
public class Livro extends Exemplar {

    private String autor;
    private String editora;
    private int edicao;

    public Livro() {
    }

    public Livro(int codigo, String titulo , String autor, String editora, int edicao) {
        super(codigo, titulo);
        this.autor = autor;
        this.editora = editora;
        this.edicao = edicao;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getEdicao() {
        return edicao;
    }

    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }

}
