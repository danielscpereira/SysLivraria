/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

/**
 *
 * @author Daniel
 */
public class Aluno extends Cliente {

    private int numeroMatricula;

    public Aluno() {
       
    }

    public Aluno(String nome, String telefone, int numeroMatricula) {
        super(nome, telefone);
        this.numeroMatricula = numeroMatricula;
    }

    public Aluno(String nome, String telefone, int numeroMatricula, String cep, String logradouro, int numero, String bairro, String cidade, String uf) {
        super(nome, telefone, cep, logradouro, numero, bairro, cidade, uf);
        this.numeroMatricula = numeroMatricula;
    }

    public int getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(int numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }
    
    

}
