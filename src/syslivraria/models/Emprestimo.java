/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class Emprestimo extends Entidade {

    private Cliente cliente;
    private final List<Exemplar> exemplares;
    private final Date dataEmprestimo;
    private Date dataDevolucao;
    private final int taxaPorDia = 1 ; 

    public Emprestimo(Date dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
        this.exemplares = new ArrayList<>();
    }

    public Emprestimo(Cliente cliente, Date dataEmprestimo) {
        this.cliente = cliente;
        this.exemplares = new ArrayList<>();
        this.dataEmprestimo = dataEmprestimo;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public List<Exemplar> getExemplares() {
        return exemplares;
    }

    public Date getDataEmprestimo() {
        return dataEmprestimo;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public void addExemplar(Exemplar exemplar) {
        this.exemplares.add(exemplar);
    }

    public void removeExemplar(Exemplar exemplar) {
        this.exemplares.remove(exemplar);
    }
    
    public double getDias() {

        Date dataAtual = new Date();

        long differenceMilliSeconds = dataAtual.getTime() - this.dataEmprestimo.getTime();
        int dias = (int)(differenceMilliSeconds / 1000 / 60 / 60 / 24);

        return dias ;
    }

    public double getMulta() {

        return this.getDias() * taxaPorDia ;
    }

    //diferenca entre datas 
    public static void main(String args[]) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");

        try {
            Date date1 = sdf.parse("05/09/2003");
            Date date2 = sdf.parse("06/09/2003");
            long differenceMilliSeconds = date2.getTime() - date1.getTime();
            System.out.println("diferenca em milisegundos: " + differenceMilliSeconds);
            System.out.println("diferenca em segundos: " + (differenceMilliSeconds / 1000));
            System.out.println("diferenca em minutos: " + (differenceMilliSeconds / 1000 / 60));
            System.out.println("diferenca em horas: " + (differenceMilliSeconds / 1000 / 60 / 60));
            System.out.println("diferenca em dias: " + (differenceMilliSeconds / 1000 / 60 / 60 / 24));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
