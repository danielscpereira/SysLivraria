/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

/**
 *
 * @author Daniel
 */
public class Periodico extends Exemplar {

    private String editora;

    public Periodico() {
    }

    public Periodico(int codigo, String titulo, String editora) {
        super(codigo, titulo);
        this.editora = editora;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

}
