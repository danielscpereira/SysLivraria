/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

/**
 *
 * @author Daniel
 */
public abstract class Exemplar extends Entidade {

    private int codigo;
    private String titulo;

    public Exemplar() {
    }

    public Exemplar(int codigo, String titulo) {
        this.codigo = codigo;
        this.titulo = titulo;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    
    

}
