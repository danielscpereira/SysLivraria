/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syslivraria.models;

/**
 *
 * @author Daniel
 */
public abstract class Cliente extends Entidade {

    private String nome;
    private String telefone;
    private Endereco endereco;

    public Cliente() {
        this.endereco = new Endereco();
    }

    public Cliente(String nome, String telefone) {
        this();
        this.nome = nome;
        this.telefone = telefone;
    }

    public Cliente(String nome, String telefone, String cep, String logradouro, int numero, String bairro, String cidade, String uf) {
        this(nome, telefone);
        this.endereco = new Endereco(cep, logradouro, numero, bairro, cidade, uf);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

}
